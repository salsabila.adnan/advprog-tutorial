package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public interface Armor {

    String getName();

    String getDescription();
}
