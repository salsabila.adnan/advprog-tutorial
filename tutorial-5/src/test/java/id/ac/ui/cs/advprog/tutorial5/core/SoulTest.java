package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        this.soul = new Soul(1, "Salsa", 20, "F", "Mahasiswa");
    }

    @Test
    public void testGetName() {
        assertEquals("Salsa", this.soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(20, this.soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals("F", this.soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Mahasiswa", soul.getOccupation());
    }

    @Test
    public void testsetName() {
        soul.setName("Yasmin");
        assertEquals("Yasmin", soul.getName());
    }

    @Test
    public void testsetGender() {
        soul.setGender("M");
        assertEquals("M", soul.getGender());
    }

    @Test
    public void testsetAge() {
        soul.setAge(19);
        assertEquals(19, soul.getAge());
    }
}
