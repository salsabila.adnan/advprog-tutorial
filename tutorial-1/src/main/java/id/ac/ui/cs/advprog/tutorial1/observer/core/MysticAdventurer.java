package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
        }

        public void update(){
                String questType = this.guild.getQuestType();
                if(questType.equals("E") || questType.equals("D")){
                        getQuests().add(this.guild.getQuest());
                }
        }
}
