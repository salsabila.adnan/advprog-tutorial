package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnhanceRepositoryTest {

    ArrayList<Weapon> weapons = new ArrayList<>();
    Weapon weapon;

    @BeforeEach
    public void setUp() {
        weapons.add(new Gun());
        weapons.add(new Sword());
        weapons.add(new Shield());
        weapons.add(new Longbow());
    }

    @Test
    public void testMethodEnhanceToAllWeapons() {

        for (Weapon weapon : weapons) {

            if (weapon.getName().equals("Gun")) {

                Weapon gun;
                gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                gun = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(gun);
                gun = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(gun);
                int index = weapons.indexOf(weapon);
                weapons.set(index, gun);
                assertTrue(weapon.getWeaponValue() < gun.getWeaponValue());
            }
        }
    }
}
