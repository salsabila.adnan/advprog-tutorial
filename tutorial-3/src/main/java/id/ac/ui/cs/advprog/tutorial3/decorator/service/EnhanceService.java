package id.ac.ui.cs.advprog.tutorial3.decorator.service;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

public interface EnhanceService {

    public void enhanceAllWeapons();

    public Iterable<Weapon> getAllWeapons();
}
